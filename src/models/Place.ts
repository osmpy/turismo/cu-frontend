export type Place = {
  id: number;
  image: string;
  title: string;
  short_description: string;
  long_description: string;
  category_id: number;
  latitude: number;
  longitude: number;
};
