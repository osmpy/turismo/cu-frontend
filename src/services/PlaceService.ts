import { Category } from 'src/models/Category';
import { Place } from 'src/models/Place';
import axios from 'axios';

class PlaceService {
  async getCategories(): Promise<Array<Category>> {
    const response = await axios.get<Array<Category>>(
      `${process.env.API_URL}/api/v1/categories`
    );
    return response.data;
  }

  async getCategory(id: number): Promise<Category> {
    const response = await axios.get<Category>(
      `${process.env.API_URL}/api/v1/categories/${id}`
    );
    return response.data;
  }

  async getPlaces(): Promise<Array<Place>> {
    const response = await axios.get<Array<Place>>(
      `${process.env.API_URL}/api/v1/places`
    );
    return response.data;
  }

  async getPlace(id: number): Promise<Place> {
    const response = await axios.get<Place>(
      `${process.env.API_URL}/api/v1/places/${id}`
    );
    return response.data;
  }
}

export default new PlaceService();
